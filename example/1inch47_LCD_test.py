#!/usr/bin/python
# -*- coding: UTF-8 -*-
#import chardet
import os
import sys
import time
import logging
import spidev as SPI
sys.path.append("..")
from lib import LCD_1inch47
from PIL import Image,ImageDraw,ImageFont, ImageOps
import socket
import os
# Raspberry Pi pin configuration:
RST = 27
DC = 25
BL = 18
bus = 0
device = 0
logging.basicConfig(level=logging.DEBUG)

import requests
import json
import random
from json import load
from urllib.request import urlopen
import itertools

def printImage(image, frame):
   tmp = image
   tmp = tmp.crop(frame)
   tmp = tmp.resize((100, 100), Image.NEAREST)
   tmp = tmp.rotate(90, expand=1)
   time.sleep(0.3)
   return tmp
   
def getLocalIp():
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(("8.8.8.8", 80))
  local_ip = s.getsockname()[0]
  s.close()
  return local_ip
    
def getHostname():
  return socket.gethostname()
  
def getPublicIp(): 
  return  load(urlopen('http://httpbin.org/ip'))['origin']

def getSSID():
  return os.popen("sudo iwgetid -r").read()

def get_cpu_temp():
    result = 0.0
    if os.path.isfile('/sys/class/thermal/thermal_zone0/temp'):
        with open('/sys/class/thermal/thermal_zone0/temp') as f:
            line = f.readline().strip()
        if line.isdigit():
            result = float(line) / 1000
    prout = str(round(result, 2))
    return f'{prout}c' 

try:
    disp = LCD_1inch47.LCD_1inch47()
    disp.Init()
    disp.clear()

    image1 = Image.new("RGB", (disp.width,disp.height ), "WHITE")
    draw = ImageDraw.Draw(image1)

    draw.rectangle([(0,10*7),(172,0)], fill = "#96281b") 

    f=ImageFont.truetype("../Font/robot.ttf", 30)
    f2=ImageFont.truetype("../Font/robot.ttf", 20)
    f3=ImageFont.truetype("../Font/robot.ttf", 18)
    
    frames_loading = (
      (5, 46, 20, 61), 
      (24, 46, 39, 61), 
      (44, 46, 59, 61),
    )
    
    frames_bad = (
      (5, 65, 20, 80), 
      (23, 65, 38, 80), 
    )
    
    frames_fail = (
      (52, 65, 67, 80), 
      (71, 65, 86, 80), 
      (90, 85, 105, 80)
    )
    
    frames_face = (
      (5, 26, 20, 41), 
      (24, 26, 39, 41), 
    )
    
    frames = (
      (5, 5, 20, 20), 
      (23, 5, 38, 20), 
      (42, 5, 57, 20), 
      (61, 5, 76, 20), 
      (79, 5, 94, 20), 
    )

    image = Image.open('../pic/PC.png')
    
    while True :
      t_end = time.time() + 20
      while time.time() < t_end:
        for url in itertools.cycle(frames_loading):
          tmp = printImage(image, url)
          image1.paste( tmp, (30,10),  tmp)
          disp.ShowImage(image1)
          if time.time() > t_end:
            break
          txt=Image.new("RGBA", (disp.height, disp.width))
      d = ImageDraw.Draw(txt)
      d.text((10, 0), getLocalIp(),  font=f, fill="BLACK")
      w=txt.rotate(90,  expand=1)
      image1.paste( w, (0,0),  w)
    
      txt=Image.new("RGBA", (disp.height, disp.width))
      d = ImageDraw.Draw(txt)
      d.text((10, 25), getPublicIp(),  font=f, fill="BLACK")
      w=txt.rotate(90,  expand=1)
      image1.paste( w, (0,0),  w)
    
      txt=Image.new("RGBA", (disp.height, disp.width))
      d = ImageDraw.Draw(txt)
      d.text((10, 52), getSSID(),  font=f2, fill="RED")
      w=txt.rotate(90,  expand=1)
      image1.paste( w, (0,0),  w)

      txt=Image.new("RGBA", (disp.height, disp.width))
      d = ImageDraw.Draw(txt)
      d.text( (10, 67), getHostname(), font=f3, fill="RED")
      w=txt.rotate(90,  expand=1)
      image1.paste( w, (0,0),  w)
      
      disp.ShowImage(image1)
      
      while True:
        for url in itertools.cycle(frames):
          tmp = printImage(image, url)
          image1.paste( tmp, (30,10),  tmp)
          disp.ShowImage(image1)
          time.sleep(1)
          draw.rectangle([(87,320),(120,120)], fill = "WHITE")
          txt=Image.new("RGBA", (disp.height, disp.width))
          d = ImageDraw.Draw(txt)
          d.text( (10, 87), get_cpu_temp(), font=f, fill="RED")
          w=txt.rotate(90,  expand=1)
          image1.paste( w, (0,0),  w) 
      break

    disp.module_exit()
except IOError as e:
	logging.info(e)
except KeyboardInterrupt:
	disp.module_exit()
	logging.info("quit:")
	exit()
